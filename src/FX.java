import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

import java.util.concurrent.Delayed;

public class FX extends Application {
    private TextField textField,textFormula;
    private boolean function = false;
    public static void main(String[] args){
        launch(args);
    }

    class MyHandle implements EventHandler<ActionEvent>{

        @Override
        public void handle(ActionEvent event) {
            Button button = (Button)event.getSource();
            Alert error = new Alert(Alert.AlertType.ERROR);
            Alert type = new Alert(Alert.AlertType.INFORMATION);

            String press = button.getText();
            switch (press){
                case "C":
                    textField.setText("");
                    textFormula.setText("");
                    break;
                case "F":
                    function = !function;
                    if(function == true) {
                        type.setContentText("Radical ON.");
                        type.show();
                    }else {
                        type.setContentText("Radical OFF.");
                        type.show();
                    }
                    break;
                case  "+":
                    textField.setText("");
                    textFormula.setText(textFormula.getText().concat(press));
                    break;
                case "-":
                    textField.setText("");
                    textFormula.setText(textFormula.getText().concat(press));
                    break;
                case "*":
                    textField.setText("");
                    textFormula.setText(textFormula.getText().concat(press));
                    break;
                case "/":
                    textField.setText("");
                    textFormula.setText(textFormula.getText().concat(press));
                    break;
                case ".":
                    textField.setText(textField.getText().concat(press));
                    textFormula.setText(textFormula.getText().concat(press));
                    break;
                case "=":
                    Expression expression = new ExpressionBuilder(textFormula.getText()).build();
                    textField.setText(String.valueOf(expression.evaluate()));
                    break;
                case "√":
                    if (function == true) {
                        Expression expression1 = new ExpressionBuilder(textFormula.getText()).build();
                        textField.setText(String.valueOf(Math.sqrt(Double.valueOf(expression1.evaluate()))));
                        textFormula.setText("");
                    }else
                    {
                        error.setContentText("Function Error!");
                        error.show();
                    }
                    break;
                default:
                    textField.setText(textField.getText().concat(press));
                    textFormula.setText(textFormula.getText().concat(press));
            }
        }
    }

    class MyKeyHandler implements EventHandler<KeyEvent>{

        @Override
        public void handle(KeyEvent event) {
            System.out.println(event.getCode());
            String current = textField.getText();
            String currents = textFormula.getText();
            switch (event.getCode()){
                case DIGIT0:
                case NUMPAD0:
                    textField.setText(current.concat("0"));
                    textFormula.setText(currents.concat("0"));
                    break;
                case DIGIT1:
                case NUMPAD1:
                    textField.setText(current.concat("1"));
                    textFormula.setText(currents.concat("1"));
                    break;
                case DIGIT2:
                case NUMPAD2:
                    textField.setText(current.concat("2"));
                    textFormula.setText(currents.concat("2"));
                    break;
                case DIGIT3:
                case NUMPAD3:
                    textField.setText(current.concat("3"));
                    textFormula.setText(currents.concat("3"));
                    break;
                case DIGIT4:
                case NUMPAD4:
                    textField.setText(current.concat("4"));
                    textFormula.setText(currents.concat("4"));
                    break;
                case DIGIT5:
                case NUMPAD5:
                    textField.setText(current.concat("5"));
                    textFormula.setText(currents.concat("5"));
                    break;
                case DIGIT6:
                case NUMPAD6:
                    textField.setText(current.concat("6"));
                    textFormula.setText(currents.concat("6"));
                    break;
                case DIGIT7:
                case NUMPAD7:
                    textField.setText(current.concat("7"));
                    textFormula.setText(currents.concat("7"));
                    break;
                case DIGIT8:
                case NUMPAD8:
                    textField.setText(current.concat("8"));
                    textFormula.setText(currents.concat("8"));
                    break;
                case DIGIT9:
                case NUMPAD9:
                    textField.setText(current.concat("9"));
                    textFormula.setText(currents.concat("9"));
                    break;
                case BACK_SPACE:
                case DELETE:
                    textField.setText("");
                    textFormula.setText("");
                    break;
                case DECIMAL:
                    textField.setText(current.concat("."));
                    textFormula.setText(currents.concat("."));
                    break;
                case ADD:
                    textField.setText("");
                    textFormula.setText(currents.concat("+"));
                    break;
                case SUBTRACT:
                    textField.setText("");
                    textFormula.setText(currents.concat("-"));
                    break;
                case MULTIPLY:
                    textField.setText("");
                    textFormula.setText(currents.concat("*"));
                    break;
                case DIVIDE:
                    textField.setText("");
                    textFormula.setText(currents.concat("/"));
                    break;
                case ENTER:
                case EQUALS:
                    Expression expression = new ExpressionBuilder(textFormula.getText()).build();
                    textField.setText(String.valueOf(expression.evaluate()));
                    textFormula.setText("");
                    break;
            }
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        MyHandle myHandle = new MyHandle();
        MyKeyHandler myKeyHandler = new MyKeyHandler();

        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.requestFocus();
        gridPane.setHgap(10);
        gridPane.setVgap(5);

        textField = new TextField();
        textField.setEditable(false);
        textField.setOnKeyPressed(myKeyHandler);

        textFormula = new TextField();
        textFormula.setEditable(false);
        textFormula.setOnKeyPressed(myKeyHandler);

        Button btnOne = new Button("1");
        btnOne.setPrefSize(75,30);
        btnOne.setOnAction(myHandle);
        Button btnTwo = new Button("2");
        btnTwo.setPrefSize(75,30);
        btnTwo.setOnAction(myHandle);
        Button btnThree = new Button("3");
        btnThree.setPrefSize(75,30);
        btnThree.setOnAction(myHandle);
        Button btnFour = new Button("4");
        btnFour.setPrefSize(75,30);
        btnFour.setOnAction(myHandle);
        Button btnFive = new Button("5");
        btnFive.setPrefSize(75,30);
        btnFive.setOnAction(myHandle);
        Button btnSix = new Button("6");
        btnSix.setPrefSize(75,30);
        btnSix.setOnAction(myHandle);
        Button btnSeven = new Button("7");
        btnSeven.setPrefSize(75,30);
        btnSeven.setOnAction(myHandle);
        Button btnEight = new Button("8");
        btnEight.setPrefSize(75,30);
        btnEight.setOnAction(myHandle);
        Button btnNine = new Button("9");
        btnNine.setPrefSize(75,30);
        btnNine.setOnAction(myHandle);
        Button btnZero = new Button("0");
        btnZero.setPrefSize(150,30);
        btnZero.setOnAction(myHandle);
        Button btnPlus = new Button("+");
        btnPlus.setPrefSize(75,30);
        btnPlus.setOnAction(myHandle);
        Button btnMinus = new Button("-");
        btnMinus.setPrefSize(75,30);
        btnMinus.setOnAction(myHandle);
        Button btnStar = new Button("*");
        btnStar.setPrefSize(75,30);
        btnStar.setOnAction(myHandle);
        Button btnDivide = new Button("/");
        btnDivide.setPrefSize(75,30);
        btnDivide.setOnAction(myHandle);
        Button btnDot = new Button(".");
        btnDot.setPrefSize(75,30);
        btnDot.setOnAction(myHandle);
        Button btnEqual = new Button("=");
        btnEqual.setPrefSize(75,30);
        btnEqual.setOnAction(myHandle);
        Button btnClear = new Button("C");
        btnClear.setPrefSize(75,30);
        btnClear.setOnAction(myHandle);
        Button btnFunction = new Button("F");
        btnFunction.setPrefSize(75,30);
        btnFunction.setOnAction(myHandle);
        Button btnRadical = new Button("√");
        btnRadical.setPrefSize(75,30);
        btnRadical.setOnAction(myHandle);

        gridPane.setPadding(new Insets(10,10,10,10));
        gridPane.add(textFormula,0,0,4,1);
        gridPane.add(textField,0,0,4,1);
        gridPane.add(btnClear,0,1);
        gridPane.add(btnFunction,1,1);
        gridPane.add(btnRadical,2,1);
        gridPane.add(btnPlus,3,1);
        gridPane.add(btnSeven,0,2);
        gridPane.add(btnEight,1,2);
        gridPane.add(btnNine,2,2);
        gridPane.add(btnMinus,3,2);
        gridPane.add(btnFour,0,3);
        gridPane.add(btnFive,1,3);
        gridPane.add(btnSix,2,3);
        gridPane.add(btnStar,3,3);
        gridPane.add(btnOne,0,4);
        gridPane.add(btnTwo,1,4);
        gridPane.add(btnThree,2,4);
        gridPane.add(btnDivide,3,4);
        gridPane.add(btnZero,0,5,2,1);
        gridPane.add(btnDot,2,5);
        gridPane.add(btnEqual,3,5);

        Scene scene = new Scene(gridPane);
        primaryStage.setHeight(250);
        primaryStage.setWidth(200);
        primaryStage.setTitle("My Intelligent Calculator");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
